<?php

/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Model\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\DataObject;

class SaveSettings implements ObserverInterface
{
    protected $_scopeConfig;
    protected $tabs = false;
    protected $_objectManager;
    protected $_request;
    protected $_layout;
    protected $_storeManager;
    protected $_category;
    protected $_product;
    protected $_helper;

    public function __construct(
        \Magento\Framework\View\Layout $layout,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory $category,
        \Itoris\AutoRelatedUpsellsCrossSells\Model\ProductFactory $product,
        \Itoris\AutoRelatedUpsellsCrossSells\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_layout = $layout;
        $this->_objectManager = $objectManager;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_category = $category;
        $this->_product = $product;
        $this->_helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        if(
            !$this->_request->getParam('select_enabled_related') && !$this->_request->getParam('related_list') &&
            !$this->_request->getParam('number_related') && !$this->_request->getParam('price_range_related_1') &&
            !$this->_request->getParam('price_range_related_2') && !$this->_request->getParam('select_category_related') ||

            !$this->_request->getParam('select_enabled_upsells') && !$this->_request->getParam('upsells_list') &&
            !$this->_request->getParam('number_upsells') && !$this->_request->getParam('price_range_upsells_1') &&
            !$this->_request->getParam('price_range_upsells_2') && !$this->_request->getParam('select_category_upsells') ||

            !$this->_request->getParam('select_enabled_crosssells') && !$this->_request->getParam('crosssells_list') &&
            !$this->_request->getParam('number_crosssells') && !$this->_request->getParam('price_range_crosssells_1') &&
            !$this->_request->getParam('price_range_crosssells_2') && !$this->_request->getParam('select_category_crosssells')
        ){
            return;
        }
        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $store_id = $this->_request->getParam('store');

        /** @var \Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory $categoryModel */
        //number_related_check
        //      \Zend_Debug::dump($this->_request->getParam('price_range_related_1'));

  //      \Zend_Debug::dump($this->_request->getParam('product_category_related')); die;
        $categoryModel = $this->_category->create();
        $categoryCollection = $categoryModel->getCollection();
        $isCategory = $categoryCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '1')->addFieldToFilter('store_id', $store_id)->getData();

//==========================================     Related     =================================================================
        if ( !$this->_request->getParam('select_category_related') && $this->_request->getParam('product_category_related')) {

            if ($isCategory) {
                foreach ($isCategory as $item ){
                    $categoryModel->load($item['entity_id'])->delete();
                }
            }

            $p_c_r = array_unique( explode(',', $this->_request->getParam('product_category_related')) );
            foreach ( $p_c_r as $item ) {
                $categoryModel = $this->_category->create();
                $categoryModel->setCategoryId($item)
                    ->setProductId($this->_request->getParam('id'))
                    ->setSellsId(1)
                    ->setStoreId($store_id)
                    ->save();
            }
        }

        if( $this->_request->getParam('select_category_related') && $isCategory ){

            foreach ($isCategory as $item ){
                $categoryModel->load($item['entity_id'])->delete();
            }
        }

        $productModel = $this->_product->create();
        $productCollection = $productModel->getCollection();
        $isProduct = $productCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '1')->addFieldToFilter('store_id', $store_id)->getData();

        $attrProductArray = [];
        $attrItemIdArray = [];
        foreach($productCollection->getData() as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
            $attrItemIdArray[$item['attr_code']] = $item['item_id'];
        }

  //      \Zend_Debug::dump( $attrProductArray ); die;

        $attrRegistry = $this->_helper->getSettings($store_id)['related_enabled'];
        $attrPOST = $this->_request->getParam('select_enabled_related');
        $attrCheckPOST = $this->_request->getParam('select_enabled_related_check');

        if( isset($attrProductArray['auto_random_enabled']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['auto_random_enabled']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['auto_random_enabled'] ){
                $this->_removeItem($attrItemIdArray['auto_random_enabled']);
                $this->_saveItem('auto_random_enabled', $attrPOST, '1');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('auto_random_enabled', $attrPOST, '1');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['related_list'];
        $attrPOST = $this->_request->getParam('list_related');
        $attrCheckPOST = $this->_request->getParam('list_related_check');

        if( isset($attrProductArray['bestsell_number']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['bestsell_number']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['bestsell_number'] ){
                $this->_removeItem($attrItemIdArray['bestsell_number']);
                $this->_saveItem('bestsell_number', $attrPOST, '1');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('bestsell_number', $attrPOST, '1');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['related_number'];
        $attrPOST = $this->_request->getParam('number_related');
        $attrCheckPOST = $this->_request->getParam('number_related_check');

        if( isset($attrProductArray['limit']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['limit']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['limit'] ){
                $this->_removeItem($attrItemIdArray['limit']);
                $this->_saveItem('limit', $attrPOST, '1');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('limit', $attrPOST, '1');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['related_price_range'];
        $attrPOST_1 = $this->_request->getParam('price_range_related_1');
        $attrPOST_2 = $this->_request->getParam('price_range_related_2');
        $attrPOST = $attrPOST_1.','.$attrPOST_2;
        $attrCheckPOST = $this->_request->getParam('price_range_related_check');

        if( isset($attrProductArray['price_from']) && isset($attrProductArray['price_to']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['price_from']);
                $this->_removeItem($attrItemIdArray['price_to']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['price_from'].','.$attrProductArray['price_to'] ){
                $this->_removeItem($attrItemIdArray['price_from']);
                $this->_saveItem('price_from', $attrPOST_1, '1');
                $this->_removeItem($attrItemIdArray['price_to']);
                $this->_saveItem('price_to', $attrPOST_2, '1');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('price_from', $attrPOST_1, '1');
                $this->_saveItem('price_to', $attrPOST_2, '1');
            }
        }

        $attrPOST = $this->_request->getParam('select_category_related');

        if( !$attrPOST && !isset($attrProductArray['same_category']) ) {
            $this->_saveItem('same_category', $attrPOST, '1');
        }elseif($attrPOST && isset($attrProductArray['same_category'])){
            $this->_removeItem($attrItemIdArray['same_category']);
        }

//==========================================     UpSells    =================================================================
        $categoryModel = $this->_category->create();
        $categoryCollection = $categoryModel->getCollection();
        $isCategory = $categoryCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '2')->addFieldToFilter('store_id', $store_id)->getData();

        if ( !$this->_request->getParam('select_category_upsells')  && $this->_request->getParam('product_category_upsells') ) {

            if ($isCategory) {
                foreach ($isCategory as $item ){
                    $categoryModel->load($item['entity_id'])->delete();
                }
            }

            $p_c_r = array_unique( explode(',', $this->_request->getParam('product_category_upsells')) );
            foreach ( $p_c_r as $item ) {
                $categoryModel = $this->_category->create();
                $categoryModel->setCategoryId($item)
                    ->setProductId($this->_request->getParam('id'))
                    ->setSellsId(2)
                    ->setStoreId($store_id)
                    ->save();
            }
        }

        if( $this->_request->getParam('select_category_upsells') && $isCategory ){

            foreach ($isCategory as $item ){
                $categoryModel->load($item['entity_id'])->delete();
            }
        }

        $productModel = $this->_product->create();
        $productCollection = $productModel->getCollection();
        $isProduct = $productCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '2')->addFieldToFilter('store_id', $store_id)->getData();

        $attrProductArray = [];
        $attrItemIdArray = [];
        foreach($productCollection->getData() as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
            $attrItemIdArray[$item['attr_code']] = $item['item_id'];
        }

        //      \Zend_Debug::dump( $attrProductArray ); die;

        $attrRegistry = $this->_helper->getSettings($store_id)['upsells_enabled'];
        $attrPOST = $this->_request->getParam('select_enabled_upsells');
        $attrCheckPOST = $this->_request->getParam('select_enabled_upsells_check');

        if( isset($attrProductArray['auto_random_enabled']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['auto_random_enabled']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['auto_random_enabled'] ){
                $this->_removeItem($attrItemIdArray['auto_random_enabled']);
                $this->_saveItem('auto_random_enabled', $attrPOST, '2');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('auto_random_enabled', $attrPOST, '2');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['upsells_list'];
        $attrPOST = $this->_request->getParam('list_upsells');
        $attrCheckPOST = $this->_request->getParam('list_upsells_check');

        if( isset($attrProductArray['bestsell_number']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['bestsell_number']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['bestsell_number'] ){
                $this->_removeItem($attrItemIdArray['bestsell_number']);
                $this->_saveItem('bestsell_number', $attrPOST, '2');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('bestsell_number', $attrPOST, '2');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['upsells_number'];
        $attrPOST = $this->_request->getParam('number_upsells');
        $attrCheckPOST = $this->_request->getParam('number_upsells_check');

        if( isset($attrProductArray['limit']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['limit']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['limit'] ){
                $this->_removeItem($attrItemIdArray['limit']);
                $this->_saveItem('limit', $attrPOST, '2');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('limit', $attrPOST, '2');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['upsells_price_range'];
        $attrPOST_1 = $this->_request->getParam('price_range_upsells_1');
        $attrPOST_2 = $this->_request->getParam('price_range_upsells_2');
        $attrPOST = $attrPOST_1.','.$attrPOST_2;
        $attrCheckPOST = $this->_request->getParam('price_range_upsells_check');

        if( isset($attrProductArray['price_from']) && isset($attrProductArray['price_to']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['price_from']);
                $this->_removeItem($attrItemIdArray['price_to']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['price_from'].','.$attrProductArray['price_to'] ){
                $this->_removeItem($attrItemIdArray['price_from']);
                $this->_saveItem('price_from', $attrPOST_1, '2');
                $this->_removeItem($attrItemIdArray['price_to']);
                $this->_saveItem('price_to', $attrPOST_2, '2');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('price_from', $attrPOST_1, '2');
                $this->_saveItem('price_to', $attrPOST_2, '2');
            }
        }

        $attrPOST = $this->_request->getParam('select_category_upsells');

        if( !$attrPOST && !isset($attrProductArray['same_category']) ) {
            $this->_saveItem('same_category', $attrPOST, '2');
        }elseif($attrPOST && isset($attrProductArray['same_category'])){
            $this->_removeItem($attrItemIdArray['same_category']);
        }
//==========================================     CrossSells    =================================================================
        $categoryModel = $this->_category->create();
        $categoryCollection = $categoryModel->getCollection();
        $isCategory = $categoryCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '3')->addFieldToFilter('store_id', $store_id)->getData();

        if ( !$this->_request->getParam('select_category_crosssells')  && $this->_request->getParam('product_category_crosssells')  ) {

            if ($isCategory) {
                foreach ($isCategory as $item ){
                    $categoryModel->load($item['entity_id'])->delete();
                }
            }

            $p_c_r = array_unique( explode(',', $this->_request->getParam('product_category_crosssells')) );
            foreach ( $p_c_r as $item ) {
                $categoryModel = $this->_category->create();
                $categoryModel->setCategoryId($item)
                    ->setProductId($this->_request->getParam('id'))
                    ->setSellsId(3)
                    ->setStoreId($store_id)
                    ->save();
            }
        }

        if( $this->_request->getParam('select_category_crosssells') && $isCategory ){

            foreach ($isCategory as $item ){
                $categoryModel->load($item['entity_id'])->delete();
            }
        }

        $productModel = $this->_product->create();
        $productCollection = $productModel->getCollection();
        $isProduct = $productCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '3')->addFieldToFilter('store_id', $store_id)->getData();

        $attrProductArray = [];
        $attrItemIdArray = [];
        foreach($productCollection->getData() as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
            $attrItemIdArray[$item['attr_code']] = $item['item_id'];
        }

        //      \Zend_Debug::dump( $attrProductArray ); die;

        $attrRegistry = $this->_helper->getSettings($store_id)['crosssells_enabled'];
        $attrPOST = $this->_request->getParam('select_enabled_crosssells');
        $attrCheckPOST = $this->_request->getParam('select_enabled_crosssells_check');

        if( isset($attrProductArray['auto_random_enabled']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['auto_random_enabled']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['auto_random_enabled'] ){
                $this->_removeItem($attrItemIdArray['auto_random_enabled']);
                $this->_saveItem('auto_random_enabled', $attrPOST, '3');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('auto_random_enabled', $attrPOST, '3');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['crosssells_list'];
        $attrPOST = $this->_request->getParam('list_crosssells');
        $attrCheckPOST = $this->_request->getParam('list_crosssells_check');

        if( isset($attrProductArray['bestsell_number']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['bestsell_number']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['bestsell_number'] ){
                $this->_removeItem($attrItemIdArray['bestsell_number']);
                $this->_saveItem('bestsell_number', $attrPOST, '3');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('bestsell_number', $attrPOST, '3');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['crosssells_number'];
        $attrPOST = $this->_request->getParam('number_crosssells');
        $attrCheckPOST = $this->_request->getParam('number_crosssells_check');

        if( isset($attrProductArray['limit']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['limit']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['limit'] ){
                $this->_removeItem($attrItemIdArray['limit']);
                $this->_saveItem('limit', $attrPOST, '3');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('limit', $attrPOST, '3');
            }
        }

        $attrRegistry = $this->_helper->getSettings($store_id)['crosssells_price_range'];
        $attrPOST_1 = $this->_request->getParam('price_range_crosssells_1');
        $attrPOST_2 = $this->_request->getParam('price_range_crosssells_2');
        $attrPOST = $attrPOST_1.','.$attrPOST_2;
        $attrCheckPOST = $this->_request->getParam('price_range_crosssells_check');

        if( isset($attrProductArray['price_from']) && isset($attrProductArray['price_to']) ) {
            if ( (!$attrCheckPOST && $attrRegistry == $attrPOST) || $attrCheckPOST ) {
                $this->_removeItem($attrItemIdArray['price_from']);
                $this->_removeItem($attrItemIdArray['price_to']);
            }elseif( !$attrCheckPOST && $attrPOST != $attrProductArray['price_from'].','.$attrProductArray['price_to'] ){
                $this->_removeItem($attrItemIdArray['price_from']);
                $this->_saveItem('price_from', $attrPOST_1, '3');
                $this->_removeItem($attrItemIdArray['price_to']);
                $this->_saveItem('price_to', $attrPOST_2, '3');
            }
        }else{
            if ( !$attrCheckPOST && $attrRegistry != $attrPOST ) {
                $this->_saveItem('price_from', $attrPOST_1, '3');
                $this->_saveItem('price_to', $attrPOST_2, '3');
            }
        }

        $attrPOST = $this->_request->getParam('select_category_crosssells');

        if( !$attrPOST && !isset($attrProductArray['same_category']) ) {
            $this->_saveItem('same_category', $attrPOST, '3');
        }elseif($attrPOST && isset($attrProductArray['same_category'])){
            $this->_removeItem($attrItemIdArray['same_category']);
        }

    }

    private function _saveItem( $attr_code, $value, $sells_id )
    {
        $productModel = $this->_product->create();
        $productModel->setProductId($this->_request->getParam('id'))
            ->setSellsId($sells_id)
            ->setStoreId($this->_request->getParam('store'))
            ->setValue($value)
            ->setAttrCode($attr_code)
            ->save();


    }

    private function _removeItem($attrId)
    {
        $productModel = $this->_product->create();
        $productModel->load($attrId);
        $productModel->delete();
    }
}