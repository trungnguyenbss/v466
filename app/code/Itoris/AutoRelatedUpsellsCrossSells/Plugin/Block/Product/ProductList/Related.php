<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Plugin\Block\Product\ProductList;

class Related
{
    protected $_request;
    protected $_categoryItoris;
    protected $_productItoris;
    protected $_product;
    protected $_category;
    protected $_registry;
    protected $_itemFactory;
    protected $_helper;
    protected $_productRepository;
    protected $_productStatus;
    protected $_productVisibility;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory $categoryItoris,
        \Itoris\AutoRelatedUpsellsCrossSells\Model\ProductFactory $productItoris,
        \Itoris\AutoRelatedUpsellsCrossSells\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\CategoryFactory $category,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->_productVisibility = $productVisibility;
        $this->_productStatus = $productStatus;
        $this->_request = $request;
        $this->_productRepository = $productRepository;
        $this->_registry = $registry;
        $this->_categoryItoris = $categoryItoris;
        $this->_category = $category;
        $this->_productItoris = $productItoris;
        $this->_product = $product;
        $this->_helper = $helper;
        $this->_itemFactory = $itemFactory;
        $this->_storeManager = $storeManager;
    }

    public function aroundGetItems($subject, $procede)
    {
        $productListDefault = $procede();
        if(!$this->_helper->isEnabled()){
            return $productListDefault;
        }

        $store_id = $this->_storeManager->getStore()->getId();

        if( $this->_registry->registry('itoris_related_upsells_crosssels___related') ){
            return  $this->_registry->registry('itoris_related_upsells_crosssels___related');
        }

        /** @var $productModel \Magento\Catalog\Model\Product */
        $productModel = $this->_product->create();
        $productCollection = $productModel->getCollection();

        $categoryItorisModel = $this->_categoryItoris->create();
        $categoryItorisCollection = $categoryItorisModel->getCollection();
        $isCategoryItoris = $categoryItorisCollection->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('sells_id', '1')->addFieldToFilter('sells_id', '1')->addFieldToFilter( 'store_id',["in"=>[$store_id, 0]] )->getData();

        if($isCategoryItoris) {
            $arrBuff = [];
            foreach ($isCategoryItoris as $item){
                array_push($arrBuff, $item['category_id']);
                }
            $categoryItoris['category_id'] = $arrBuff;

        }else{
            $categoryItoris['category_id'] = [1];
        }

        $productItorisModel = $this->_productItoris->create();
        $productItorisCollection = $productItorisModel->getCollection();
        $isProductItoris = $productItorisCollection->addFieldToFilter( 'product_id', $this->_request->getParam('id') )
            ->addFieldToFilter('sells_id', '1')->addFieldToFilter( 'store_id', ["in"=>[$store_id]] );


        if( !$isProductItoris ->getData() ){
            $productItorisModel = $this->_productItoris->create();
            $isProductItoris  = $productItorisModel->getCollection()
                ->addFieldToFilter('product_id', $this->_request->getParam('id'))
                ->addFieldToFilter('sells_id', '1')
                ->addFieldToFilter('store_id', ["in"=>[0]]);
        }

        $attrProductArray = [];
        $productParam = [];
        foreach($productItorisCollection->getData() as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
        }

        if( isset($attrProductArray['auto_random_enabled']) ){
            $productParam['enabled'] = $attrProductArray['auto_random_enabled'];
        }else{
            $productParam['enabled'] = $this->_helper->getFormDataRelated($store_id)['enabled'];
        }

        if( isset($attrProductArray['bestsell_number']) ){
            $productParam['top_bestsellers'] = $attrProductArray['bestsell_number'];
        }else{
            $productParam['top_bestsellers'] = $this->_helper->getFormDataRelated($store_id)['top_bestsellers'];
        }

        if(isset($attrProductArray['price_from']) && isset($attrProductArray['price_to'])){
            $productParam['price_range'] = $attrProductArray['price_from'] .','. $attrProductArray['price_to'];
        }else{
            $productParam['price_range'] = $this->_helper->getFormDataRelated($store_id)['price_range'];
        }

        if( isset($attrProductArray['limit']) ){
            $productParam['limit_number'] = $attrProductArray['limit'];
        }else{
            $productParam['limit_number'] = $this->_helper->getFormDataRelated($store_id)['limit_number'];
        }

        if( isset($attrProductArray['same_category']) ){
            $productParam['use_same_categories'] = 0;
        }else{
            $productParam['use_same_categories'] = 1;
        }

        if( $productListDefault->getData() )
            $isProductListDefault = true;
        else
            $isProductListDefault = false;

        $product_id = $this->_request->getParam('id');

        $product = $this->_product->create()->load($product_id);

        if($isCategoryItoris) {


            $categoryIds =  $categoryItoris['category_id'] ;
        }else{
            $categoryIds = $product->getCategoryIds();
        }

            if ( !$productParam['enabled'] || ($productParam['enabled'] == 2 && $isProductListDefault) ){
                return $productListDefault;
            }else {

                if($productParam['use_same_categories']) {

                    $categories = $this->_category->create()->getCollection();
                    $categoryIds = $categories
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('is_active', 1)
                        ->addAttributeToFilter('include_in_menu', 1)
                        ->addFieldToFilter('entity_id', ['in' => $categoryIds])
                        ->addFieldToFilter('level', ['gt' => 1])->load()->getAllIds();
                }

                $productCollection->addCategoriesFilter(['in' => $categoryIds]);

                $productCollection->addStoreFilter( $this->_storeManager->getStore() );
                $productCollection->addAttributeToSelect('*');

                $productCollection->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()]);
                $productCollection->setVisibility($this->_productVisibility->getVisibleInSiteIds());

                $price_min = (1 + explode(',', $productParam['price_range'])[0] / 100) * ( $product->getData('price') );
                $price_max = (1 + explode(',', $productParam['price_range'])[1] / 100) * ( $product->getData('price') );

                $productCollection
                    ->addFieldToFilter('price', ['from' => $price_min, 'to' => $price_max]);

                $productCollection->load();

                $colIds = $productCollection->getAllIds();

                $afterDate = date('Y-m-d H:i:s', time()-3*3700000);

                $orderCollection = $this->_itemFactory->create()->getCollection();
                $orderCollection->addFieldToFilter('updated_at', ['gt' => $afterDate])
                    ->addFieldToFilter('product_id', ['in' =>  $colIds]);
                $orderCollection->getSelect()->from('',['count_product' => 'COUNT(main_table.product_id)'])
                    ->group('main_table.product_id')
                    ->order('count_product DESC')
                    ->limit($productParam['top_bestsellers']);
                $orderCollection->load();
                $orderIds = $orderCollection->getColumnValues('product_id');

                if( count($orderIds)< $productParam['top_bestsellers'] &&  count($orderIds)<count($colIds) ){
                    $buffArr = array_diff($colIds, $orderIds);

                    if( (count($orderIds)+count($buffArr)) < $productParam['top_bestsellers'] ){
                        $orderIds = array_merge($orderIds, $buffArr);
                    }else{
                        $keyBuff = array_rand($buffArr, $productParam['top_bestsellers'] - count($orderIds));
                        foreach ($keyBuff as $item){
                            array_push($orderIds, $buffArr[$item]);
                        }
                    }

                }

                $key = array_search($this->_request->getParam('id'),$orderIds);
                if($key!==false){
                    array_splice($orderIds, $key, 1);
                }

                $collView = $productModel->getCollection();

                $collView->addFieldToFilter('entity_id', ['in' => $orderIds]);
                $collView->getSelect()->orderRand()->limit($productParam['limit_number']);
                $collView->addAttributeToSelect('*');
                $collView->load();

            }

        $this->_registry->register('itoris_related_upsells_crosssels___related', $collView);
        return $collView;

    }


}