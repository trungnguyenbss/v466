<?php

/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    public $magentoConfigTable = 'core_config_data';
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $helper = \Magento\Framework\App\ObjectManager::getInstance()->create('Itoris\AutoRelatedUpsellsCrossSells\Helper\Data');
        $setup->startSetup();

        if(!$setup->tableExists($setup->getTable('itoris_autoupsells_item'))) {
            $setup->run("
                create table {$setup->getTable('itoris_autoupsells_item')} (
                    `item_id` int unsigned not null auto_increment primary key,
                    `product_id` int unsigned not null default 0,
                    `attr_code` ENUM('auto_random_enabled','bestsell_number', 'price_from', 'price_to', 'limit', 'same_category'),
                    `store_id` smallint unsigned not null,
                    `value` smallint not null,
                    `sells_id` ENUM('1', '2', '3'),
                    foreign key (`store_id`) references {$setup->getTable('store')} (`store_id`) on delete cascade on update cascade,
                    foreign key (`product_id`) references {$setup->getTable('catalog_product_entity')} (`entity_id`) on delete cascade on update cascade
                )engine = InnoDB default charset = utf8;
            ");
            $setup->run("CREATE INDEX itoris_autoupsells_item_index ON {$setup->getTable('itoris_autoupsells_item')}(`product_id`, `sells_id`, `store_id` );");
        }

        if(!$setup->tableExists($setup->getTable('itoris_autoupsells_product_category'))) {
            $setup->run("
                create table {$setup->getTable('itoris_autoupsells_product_category')} (
                    `entity_id` int not null auto_increment primary key,
                    `product_id` int unsigned not null default 0,
                    `category_id` int unsigned not null default 0,
                    `store_id` smallint unsigned not null,
                    `sells_id` smallint unsigned not null,
                    foreign key (`store_id`) references {$setup->getTable('store')} (`store_id`) on delete cascade on update cascade,
                    foreign key (`product_id`) references {$setup->getTable('catalog_product_entity')} (`entity_id`) on delete cascade on update cascade,
                    foreign key (`category_id`) references {$setup->getTable('catalog_category_entity')} (`entity_id`) on delete cascade on update cascade
                )engine = InnoDB default charset = utf8;
            ");
            $setup->run("CREATE INDEX itoris_autoupsells_product_category_index ON {$setup->getTable('itoris_autoupsells_product_category')}(`product_id`, `sells_id`, `store_id` );");
        }

        $strInsertValue='';
        $configNote = $helper->getBackendConfig()->getValue($helper::MODULE_ENABLED);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::MODULE_ENABLED."', '1'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::SELECT_RELATED_PRODUCTS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::SELECT_RELATED_PRODUCTS."', '2'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::RANDOM_LIST_RELATED);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::RANDOM_LIST_RELATED."', '20'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::PRICE_RANGE_RELATED);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::PRICE_RANGE_RELATED."', '5,30'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::NUMBER_RELATED);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::NUMBER_RELATED."', '6'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::SELECT_UPSELLS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::SELECT_UPSELLS."', '2'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::RANDOM_LIST_UPSELLS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::RANDOM_LIST_UPSELLS."', '20'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::PRICE_RANGE_UPSELLS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::PRICE_RANGE_UPSELLS."', '5,30'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::NUMBER_UPSELLS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::NUMBER_UPSELLS."', '6'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::SELECT_CROSSSELS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::SELECT_CROSSSELS."', '2'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::RANDOM_LIST_CROSSSELS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::RANDOM_LIST_CROSSSELS."', '20'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::PRICE_RANGE_CROSSSELS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::PRICE_RANGE_CROSSSELS."', '5,30'),";
        }
        $configNote = $helper->getBackendConfig()->getValue($helper::NUMBER_CROSSSELS);
        if(!isset($configNote)){
            $strInsertValue=$strInsertValue."('".$helper::NUMBER_CROSSSELS."', '4')";
        }

        if(strlen($strInsertValue)>0) {
            $setup->run("
            INSERT INTO {$setup->getTable($this->magentoConfigTable)}
            (path, value)
            VALUES $strInsertValue
            ");
        }
        $setup->endSetup();
    }
}