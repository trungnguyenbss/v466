<?php

/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Renderer\Element;
class Range extends \Magento\Backend\Block\Widget\Form\Renderer\Element {

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element) {
        $value = $element->getValue();
        $click ='toggleValueElements(this, this.parentNode.parentNode.parentNode); ';
 //                    toggleValueElements(this, document.getElementById("price_range_related_1").parentNode);';
        $html = '<div class="admin__field field field-list_related  with-addon">
            <label class="label admin__field-label"><span>'.__($value['title_label']).'</span></label>
            <div class="admin__field-control control"><div>
            <label class="label">'.__($value['before_label']).'</label >
            <input type = "text" name="'.$element->getName().'_1" value="'.$value['value_1'].'" id="'.$element->getName().'_1" class=" input-text admin__control-text validate-no-empty validate-number" 
                style = "width: 50px;" data-form-part="'.$value['data-form-part'].'" '.$value['disabled'].'>
            <label class="label">'. __($value['middle_label']).'</label >
            <input type = "text" name="'.$element->getName().'_2" value="'.$value['value_2'].'" id="'.$element->getName().'_2"  class=" input-text admin__control-text validate-no-empty validate-number" 
                style = "width: 50px;" data-form-part="'.$value['data-form-part'].'" '.$value['disabled'].'>
            <label >'.__($value['alter_label']).'</label >
            <label class="addafter"><br>
            <label class=""><input type="checkbox" name="'.$element->getName().'_check" id="'.$element->getName().'_check"  class="use-default-controlo" data-form-part="'.$value['data-form-part'].
            '" '.$value['checked'].' onClick="'.$click.'">
            <span class="use-default-label">Use config setting</span>
            </label>
            </label>
            </div></div></div>';
        return $html;
    }

}

