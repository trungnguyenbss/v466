<?php

/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Renderer\Element;

class Legend extends \Magento\Backend\Block\Widget\Form\Renderer\Element {

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $value = $element->getValue();
        return '<strong  class="title" style=" font-size: 1.7rem; font-weight: 600;"> <span>'.$value['legend_field'].'</span></strong><br>
<label class="label">'.$value['title_field'].'</label><br><br><br>';
    }

}


