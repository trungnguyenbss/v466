<?php

/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Catalog\Product\Edit\Tab;

class UpSells  extends  \Magento\Backend\Block\Widget\Form\Generic
{
    protected $_helper;
    protected $_category;
    protected $_product;
    protected $_objectManager;

    public function __construct(
        \Itoris\AutoRelatedUpsellsCrossSells\Helper\Data $helper,
        \Magento\Backend\Block\Template\Context $context,
        \Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory $category,
        \Itoris\AutoRelatedUpsellsCrossSells\Model\ProductFactory $product,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    )
    {
        $this->_helper = $helper;
        $this->_category = $category;
        $this->_product = $product;
        parent::__construct($context, $registry, $formFactory,  $data);
    }

    protected function _prepareForm()
    {

        $store_id = $this->_storeManager->getStore()->getId();
//        \Zend_Debug::dump( $this->_helper->getFormDataRelated($store_id) ); die;

        $categoryModel = $this->_category->create();
        $categoryCollection = $categoryModel->getCollection()
            ->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('store_id', $store_id);

        $productModel = $this->_product->create();
        $productCollection = $productModel->getCollection()
            ->addFieldToFilter('product_id', $this->_request->getParam('id'))
            ->addFieldToFilter('store_id', $store_id)->addFieldToFilter('sells_id', 2);

        $attrProductArray = [];
        foreach($productCollection->getData() as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
        }


        $form = $this->_formFactory->create(
            ['data'=>[
                'id'      => 'itoris_edit_form_related',
                'action'  => $this->getUrl('*/*/edit'),
                'method'  => 'post',
                'enctype' => 'multipart/form-data'
            ]
            ]
        );


        if( isset($attrProductArray['auto_random_enabled']) ){
            $getData['enabled'] = $attrProductArray['auto_random_enabled'];
            $getData['use_config_enabled'] = 0;

        }else{
            $getData['enabled'] = $this->_helper->getFormDataUpSell($store_id)['enabled'];
            $getData['use_config_enabled'] = 1;
        }

        if( isset($attrProductArray['bestsell_number']) ){
            $getData['top_bestsellers'] = $attrProductArray['bestsell_number'];
            $getData['use_config_top_bestsellers'] = 0;
        }else{
            $getData['top_bestsellers'] = $this->_helper->getFormDataUpSell($store_id)['top_bestsellers'];
            $getData['use_config_top_bestsellers'] = 1;
        }

        if(isset($attrProductArray['price_from']) && isset($attrProductArray['price_to'])){
            $getData['price_range'] = $attrProductArray['price_from'] .','. $attrProductArray['price_to'];
            $getData['use_config_price_range'] = 0;
        }else{
            $getData['price_range'] = $this->_helper->getFormDataUpSell($store_id)['price_range'];
            $getData['use_config_price_range'] = 1;
        }

        if( isset($attrProductArray['limit']) ){
            $getData['limit_number'] = $attrProductArray['limit'];
            $getData['use_config_limit_number'] = 0;
        }else{
            $getData['limit_number'] = $this->_helper->getFormDataUpSell($store_id)['limit_number'];
            $getData['use_config_limit_number'] = 1;
        }

        if( isset($attrProductArray['same_category']) ){
            $getData['use_same_categories'] = 0;
        }else{
            $getData['use_same_categories'] = 1;
        }


        $categoryCollection = $categoryCollection->addFieldToFilter('sells_id', 2)->getData();
        if( $categoryCollection ){
            $buff = '';
            foreach ($categoryCollection as $item){
                if($buff) {
                    $buff .= ','.$item['category_id'];
                }else{
                    $buff = $item['category_id'];
                }
            }
            $getData['id_categories'] = $buff;
        }else{
            $getData['id_categories'] = 0;
        }

        //       \Zend_Debug::dump( $getData ); die;

        $fieldset = $form->addFieldset('upsells', ['legend'=> false]);

        $fieldset->addField('title_upsells', 'hidden', [
            'name'     => 'title_upsells',
            'required' => false,
            'value' => [
                'legend_field' => __('Up-Sell Products'),
                'title_field' => __('An up-sell item is offered to the customer as a pricier or higher-quality alternative to the product the customer is looking at.')
            ],
        ])->setRenderer( $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Renderer\Element\Legend') );

        $select_enabled_check = ($getData['use_config_enabled']) ? 'checked' : '';
        $fieldset->addField('select_enabled_upsells', 'select', [
            'name'     => 'select_enabled_upsells',
            'label'    => __('Auto Select Upsells Randomly'),
            'data-form-part'=> 'product_form',
            'title'    => __('Auto Select Upsells Randomly'),
            'required' => false,
            'disabled' => $getData['use_config_enabled'],
            'multiple' => false,
            'value'    => $getData['enabled'],
            'style'    => 'width: 100%;',
            'values' => [
                2 => __('Yes, if list is empty'),
                1 => __('Yes'),
                0 => __('No')
            ]
        ])->setAfterElementHtml("
         <br><label for='select_enabled_upsells_check' class='choice use-default'>
            <input type='checkbox' name='select_enabled_upsells_check' class='use-default-control' id='select_enabled_upsells_check'  
            onclick='toggleValueElements(this, this.parentNode.parentNode.parentNode)' data-form-part='product_form' ".
            $select_enabled_check ." >
            <span class='use-default-label'>".__("Use config setting")."</span>
        </label>
         ");

        $list_check = ($getData['use_config_top_bestsellers']) ? 'checked' : '';
        $fieldset->addField('list_upsells', 'text', [
            'name'     => 'list_upsells',
            'label'    => __('Choose Random from Best Sellers List Top'),
            'data-form-part'=> 'product_form',
            'title'    => __('Choose Random from Best Sellers List Top'),
            'value'    => $getData['top_bestsellers'],
            'disabled' => $getData['use_config_top_bestsellers'],
            'required' => false,
            'class'    => 'validate-no-empty validate-number',
            'style'    => 'width: 100%;',
        ])->setAfterElementHtml("
          <br><label for='list_upsells_check' class='choice use-default'>
            <input type='checkbox' name='list_upsells_check' class='use-default-control' id='list_upsells_check'  
            onclick='toggleValueElements(this, this.parentNode.parentNode.parentNode)' data-form-part='product_form' ".
            $list_check ." >
            <span class='use-default-label'>".__("Use config setting")."</span>
        </label>
         ");

        $price_range_check = ($getData['use_config_price_range']) ? 'checked' : '';
        $price_range_disabled = ($getData['use_config_price_range']) ? 'disabled' : '';
        $fieldset->addField('price_range_upsells', 'hidden', [
            'name'     => 'price_range_upsells',
            'required' => false,

            'value' => [
                'title_label' => __('Price Point of Selected Products'),
                'before_label' => __('from'),
                'middle_label' => __('% to'),
                'alter_label' => __('% Higher'),
                'data-form-part' => 'product_form',
                'disabled' => $price_range_disabled,
                'checked' => $price_range_check,
                'value_1' => explode(',', $getData['price_range'])[0],
                'value_2' => explode(',', $getData['price_range'])[1],
            ],
        ])->setRenderer( $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Renderer\Element\Range') );

        $number_check = ($getData['use_config_limit_number']) ? 'checked' : '';
        $fieldset->addField('number_upsells', 'text', [
            'name'     => 'number_upsells',
            'label'    => __('Number of Upsells'),
            'data-form-part'=> 'product_form',
            'title'    => __('Number of Upsells'),
            'value'    => $getData['limit_number'],
            'disabled' => $getData['use_config_limit_number'],
            'required' => false,
            'class'    => 'validate-no-empty validate-number',
            'style'    => 'width: 100%;',
        ])->setAfterElementHtml("
          <br><label for='number_upsells_check' class='choice use-default'>
            <input type='checkbox' name='number_upsells_check' class='use-default-control' id='number_upsells_check'  
            onclick='toggleValueElements(this, this.parentNode.parentNode.parentNode)' data-form-part='product_form' ".
            $number_check ." >
            <span class='use-default-label'>".__("Use config setting")."</span>
        </label>
         ");

        $fieldset->addField('select_category_upsells', 'select', [
            'name'     => 'select_category_upsells',
            'label'    => __('Select from the Same Category(ies)'),
            'title'    => __('Select from the Same Category(ies)'),
            'data-form-part'=> 'product_form',
            'required' => false,
            'multiple' => false,
            'value'    =>  $getData['use_same_categories'],
            'style'    => 'width: 100%;',
            'values' => [
                1 => __('Yes'),
                0 => __('No')
            ]
        ]);

        $fieldset->addField('select_category_upsells_check', 'hidden', [
            'name'     => 'select_category_upsells_check',
            'required' => false,
            'data-form-part'=> 'product_form',
            'value' => $getData['use_same_categories'],
        ]);

        $fieldset->addField('product_category_upsells', 'hidden', [
            'name'     => 'product_category_upsells',
            'required' => false,
            'data-form-part'=> 'product_form',
            'value' => $getData['id_categories'],
            //           'class'=>'product_is_required',
//            'style'    => 'width: 400px',
        ]);



        $form->setUseContainer(false);
        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getObjectManager(){
        if($this->_objectManager)
            return $this->_objectManager;
        return $this->_objectManager=\Magento\Framework\App\ObjectManager::getInstance();
    }

}