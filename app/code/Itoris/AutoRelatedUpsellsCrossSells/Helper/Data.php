<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Helper;

use Magento\Framework\DataObject;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SCOPE_TYPE_STORES = 'store';

    const MODULE_ENABLED = 'itoris_autorelated/general/module_enabled';
    const SELECT_RELATED_PRODUCTS  = 'itoris_autorelated/related/related_enabled';
    const RANDOM_LIST_RELATED  = 'itoris_autorelated/related/related_list';
    const PRICE_RANGE_RELATED  = 'itoris_autorelated/related/related_price_range';
    const NUMBER_RELATED  = 'itoris_autorelated/related/related_number';

    const SELECT_UPSELLS  = 'itoris_autorelated/upsells/upsells_enabled';
    const RANDOM_LIST_UPSELLS  = 'itoris_autorelated/upsells/upsells_list';
    const PRICE_RANGE_UPSELLS  = 'itoris_autorelated/upsells/upsells_price_range';
    const NUMBER_UPSELLS  = 'itoris_autorelated/upsells/upsells_number';

    const SELECT_CROSSSELS  = 'itoris_autorelated/crosssells/crosssells_enabled';
    const RANDOM_LIST_CROSSSELS  = 'itoris_autorelated/crosssells/crosssells_list';
    const PRICE_RANGE_CROSSSELS  = 'itoris_autorelated/crosssells/crosssells_price_range';
    const NUMBER_CROSSSELS  = 'itoris_autorelated/crosssells/crosssells_number';

    protected $_backendConfig;
    protected $objectManager;
    protected $_registry;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Backend\App\ConfigInterface $backendConfig
    ) {
        $this->_backendConfig = $backendConfig;
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }

    public function isEnabled() {
        return (int)$this->_backendConfig->getValue('itoris_autorelated/general/module_enabled') &&
                count(explode('|', $this->_backendConfig->getValue('itoris_core/installed/Itoris_AutoRelatedUpsellsCrossSells'))) == 2;
    }
      public function getObjectManager()
      {
          return \Magento\Framework\App\ObjectManager::getInstance();
      }

    public function getBackendConfig()
    {
        return $this->_backendConfig;
    }

    public function getSettings($store)
    {
        if($this->getRegistry()->registry('itoris_autorelated_settings')){
            return  $this->getRegistry()->registry('itoris_autorelated_settings');
        }

        $registrySettings =  new DataObject([
            'related_enabled' => $this->scopeConfig
                ->getValue(self::SELECT_RELATED_PRODUCTS, self::SCOPE_TYPE_STORES, $store),
            'related_list' => $this->scopeConfig
                ->getValue(self::RANDOM_LIST_RELATED, self::SCOPE_TYPE_STORES, $store),
            'related_price_range' => $this->scopeConfig
                ->getValue(self::PRICE_RANGE_RELATED, self::SCOPE_TYPE_STORES, $store),
            'related_number' => $this->scopeConfig
                ->getValue(self::NUMBER_RELATED, self::SCOPE_TYPE_STORES, $store),
            'upsells_enabled' => $this->scopeConfig
                ->getValue(self::SELECT_UPSELLS, self::SCOPE_TYPE_STORES, $store),
            'upsells_list' => $this->scopeConfig
                ->getValue(self::RANDOM_LIST_UPSELLS, self::SCOPE_TYPE_STORES, $store),
            'upsells_price_range' => $this->scopeConfig
                ->getValue(self::PRICE_RANGE_UPSELLS, self::SCOPE_TYPE_STORES, $store),
            'upsells_number' => $this->scopeConfig
                ->getValue(self::NUMBER_UPSELLS, self::SCOPE_TYPE_STORES, $store),
            'crosssells_enabled' => $this->scopeConfig
                ->getValue(self::SELECT_CROSSSELS, self::SCOPE_TYPE_STORES, $store),
            'crosssells_list' => $this->scopeConfig
                ->getValue(self::RANDOM_LIST_CROSSSELS, self::SCOPE_TYPE_STORES, $store),
            'crosssells_price_range' => $this->scopeConfig
                ->getValue(self::PRICE_RANGE_CROSSSELS, self::SCOPE_TYPE_STORES, $store),
            'crosssells_number' => $this->scopeConfig
                ->getValue(self::NUMBER_CROSSSELS, self::SCOPE_TYPE_STORES, $store),
        ]);
        $this->getRegistry()->register('itoris_autorelated_settings',$registrySettings);
        return $registrySettings;
    }

    public function getRegistry()
    {
        if (!$this->_registry) {
            $this->_registry = $this->getObjectManager()->get('Magento\Framework\Registry');
        }
        return $this->_registry;

    }

    public function getFormDataRelated($store)
    {
        $settings = $this->getSettings($store);

        return [
            'enabled' => $settings->getRelatedEnabled(),
            'use_config_enabled' => 1,
            'top_bestsellers' => $settings->getRelatedList(),
            'use_config_top_bestsellers' => 1,
            'price_range' => $settings->getRelatedPriceRange(),
            'use_config_price_range' => 1,
            'limit_number' => $settings->getRelatedNumber(),
            'use_config_limit_number' =>  1,
            'use_same_categories' => 1
        ];
    }

    public function getFormDataUpSell($store)
    {
        $settings = $this->getSettings($store);

        return [
            'enabled' => $settings->getUpsellsEnabled(),
            'use_config_enabled' => 1,
            'top_bestsellers' => $settings->getUpsellsList(),
            'use_config_top_bestsellers' => 1,
            'price_range' => $settings->getUpsellsPriceRange(),
            'use_config_price_range' => 1,
            'limit_number' => $settings->getUpsellsNumber(),
            'use_config_limit_number' =>  1,
            'use_same_categories' => 1
        ];
    }

    public function getFormDataCrossSells($store)
    {
        $settings = $this->getSettings($store);

        return [
            'enabled' => $settings->getCrosssellsEnabled(),
            'use_config_enabled' => 1,
            'top_bestsellers' => $settings->getCrosssellsList(),
            'use_config_top_bestsellers' => 1,
            'price_range' => $settings->getCrosssellsPriceRange(),
            'use_config_price_range' => 1,
            'limit_number' => $settings->getCrosssellsNumber(),
            'use_config_limit_number' =>  1,
            'use_same_categories' => 1
        ];
    }

}