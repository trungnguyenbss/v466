<?php

/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_M2_AUTO_RELATED_UPSELLS_CROSSSELLS
 * @copyright  Copyright (c) 2017 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

namespace Itoris\AutoRelatedUpsellsCrossSells\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductLinkInterface;
use Magento\Catalog\Api\ProductLinkRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Modal;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Catalog\Model\Product\Attribute\Source\Status;

class Related extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\Related
{

    public function modifyMeta(array $meta)
    {
        if( !$this->getDataHelper()->isEnabled() ){
            return    parent::modifyMeta( $meta);
        }
        $meta = array_replace_recursive(
            $meta,
            [
                static::GROUP_RELATED => [
                    'children' => [
                        $this->scopePrefix . 'itoris_related' => $this->addFormRelated(),
                        $this->scopePrefix . 'itoris_tree_category_related' => $this->addTreeCategoryRelated(),
                        $this->scopePrefix . static::DATA_SCOPE_RELATED => $this->getRelatedFieldset(),
                        $this->scopePrefix . 'itoris_itoris_upsells' => $this->addFormUpSells(),
                        $this->scopePrefix . 'itoris_tree_category_upsells' => $this->addTreeCategoryUpSells(),
                        $this->scopePrefix . static::DATA_SCOPE_UPSELL => $this->getUpSellFieldset(),
                        $this->scopePrefix . 'itoris_itoris_crosssells' => $this->addFormCrossSells(),
                        $this->scopePrefix . 'itoris_tree_category_crosssells' => $this->addTreeCategoryCrossSells(),
                        $this->scopePrefix . static::DATA_SCOPE_CROSSSELL => $this->getCrossSellFieldset(),
                    ],
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Related Products, Up-Sells, and Cross-Sells'),
                                'collapsible' => true,
                                'componentType' => Fieldset::NAME,
                                'dataScope' => static::DATA_SCOPE,
                                'sortOrder' =>
                                    $this->getNextGroupSortOrder(
                                        $meta,
                                        'search-engine-optimization',
                                        90
                                    ),
                            ],
                        ],

                    ],
                ],
            ]
        );

        return $meta;
    }
public function addFormRelated()
{

    /** @var \Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Catalog\Product\Edit\Tab\Related $block */
    $block = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Catalog\Product\Edit\Tab\Related');
    return [
        'children' => [
            'BPP' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => null,
                            'formElement' => \Magento\Ui\Component\Container::NAME,
                            'componentType' => \Magento\Ui\Component\Container::NAME,
                            'template' => 'ui/form/components/complex',
                            'sortOrder' => 10,
                            'content' => $block->toHtml(),
                        ],
                    ],
                ],
            ]
        ],
        'arguments' => [
            'data' => [
                'config' => [
                    'additionalClasses' => 'admin__fieldset-section',
                    'label' => false,
                    'collapsible' => false,
                    'componentType' => Fieldset::NAME,
                    'dataScope' => '',
                    'sortOrder' => 10,
                ],
            ],
        ]
    ];
}

    public function addTreeCategoryRelated()
    {
        $block = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Category\Tree');
        $categoryModelFactory = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory');
        $block->setTreeId('_itoris_1');
        $block->setTreeLinkId('_related');

        $categoryModel = $categoryModelFactory->create();
        $categoryCollection = $categoryModel->getCollection();
  //      \Zend_Debug::dump($block->getStore()->getId()); die;
        $loadCategory = $categoryCollection->addFieldToFilter( 'product_id', $block->getRequestProductId() )
            ->addFieldToFilter('sells_id', '1')->addFieldToFilter( 'store_id', $block->getStore()->getId() )->getData();

        $productModelFactory = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Model\ProductFactory');
        $productModel = $productModelFactory->create();
        $productCollection = $productModel->getCollection();
        $loadProduct = $productCollection->addFieldToFilter( 'product_id', $block->getRequestProductId() )
            ->addFieldToFilter('sells_id', '1')->addFieldToFilter( 'store_id', $block->getStore()->getId() )->getData();

        $attrProductArray = [];
        foreach( $loadProduct as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
        }

        if( isset($attrProductArray['same_category']) ){
            $block->setSameCategory(0);
        }else{
            $block->setSameCategory(1);
        }

        if($loadCategory){
            $buff = '';
            foreach ($loadCategory as $item){
                if($buff) {
                    $buff .= ','.$item['category_id'];
                }else{
                    $buff = $item['category_id'];
                }
            }
            $block->setOpenCategory( $buff );

        }else{
            $block->setOpenCategory('1');
        }

        return [
            'children' => [
                'BPP' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => null,
                                'formElement' => \Magento\Ui\Component\Container::NAME,
                                'componentType' => \Magento\Ui\Component\Container::NAME,
                                'template' => 'ui/form/components/complex',
                                'sortOrder' => 15,
                                'content' => $block->toHtml(),
                            ],
                        ],
                    ],
                ]
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 15,
                    ],
                ],
            ]
        ];
    }


    protected function getRelatedFieldset()
    {
        if(!$this->getDataHelper()->isEnabled()){
            return parent::getRelatedFieldset();
        }
        $content = __('');

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Related Products'),
                    $this->scopePrefix . static::DATA_SCOPE_RELATED
                ),
                'modal' => $this->getGenericModal(
                    __('Add Related Products'),
                    $this->scopePrefix . static::DATA_SCOPE_RELATED
                ),
                static::DATA_SCOPE_RELATED => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_RELATED),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 20,
                    ],
                ],
            ]
        ];
    }

    public function addFormUpSells()
    {
        $block = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Catalog\Product\Edit\Tab\UpSells');
        return [
            'children' => [
                'BPP' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => null,
                                'formElement' => \Magento\Ui\Component\Container::NAME,
                                'componentType' => \Magento\Ui\Component\Container::NAME,
                                'template' => 'ui/form/components/complex',
                                'sortOrder' => 25,
                                'content' => $block->toHtml(),
                            ],
                        ],
                    ],
                ]
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 25,
                    ],
                ],
            ]
        ];
    }

    public function addTreeCategoryUpSells()
    {
        $block = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Category\Tree');
        $block->setTreeId('_itoris_2');
        $block->setTreeLinkId('_upsells');


        $categoryModelFactory = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory');

        $categoryModel = $categoryModelFactory->create();
        $categoryCollection = $categoryModel->getCollection();
        //      \Zend_Debug::dump($block->getStore()->getId()); die;
        $loadCategory = $categoryCollection->addFieldToFilter( 'product_id', $block->getRequestProductId() )
            ->addFieldToFilter('sells_id', '2')->addFieldToFilter( 'store_id', $block->getStore()->getId() )->getData();

        $productModelFactory = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Model\ProductFactory');
        $productModel = $productModelFactory->create();
        $productCollection = $productModel->getCollection();
        $loadProduct = $productCollection->addFieldToFilter( 'product_id', $block->getRequestProductId() )
            ->addFieldToFilter('sells_id', '2')->addFieldToFilter( 'store_id', $block->getStore()->getId() )->getData();

        $attrProductArray = [];
        foreach( $loadProduct as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
        }

        if( isset($attrProductArray['same_category']) ){
            $block->setSameCategory(0);
        }else{
            $block->setSameCategory(1);
        }

        if($loadCategory){
            $buff = '';
            foreach ($loadCategory as $item){
                if($buff) {
                    $buff .= ','.$item['category_id'];
                }else{
                    $buff = $item['category_id'];
                }
            }
            $block->setOpenCategory( $buff );

        }else{
            $block->setOpenCategory('1');
        }

        return [
            'children' => [
                'BPP' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => null,
                                'formElement' => \Magento\Ui\Component\Container::NAME,
                                'componentType' => \Magento\Ui\Component\Container::NAME,
                                'template' => 'ui/form/components/complex',
                                'sortOrder' => 30,
                                'content' => $block->toHtml(),
                            ],
                        ],
                    ],
                ]
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 30,
                    ],
                ],
            ]
        ];
    }

    protected function getUpSellFieldset()
    {
        if(!$this->getDataHelper()->isEnabled()){
            return parent::getUpSellFieldset();
        }
        $content = __('');

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Up-Sell Products'),
                    $this->scopePrefix . static::DATA_SCOPE_UPSELL
                ),
                'modal' => $this->getGenericModal(
                    __('Add Up-Sell Products'),
                    $this->scopePrefix . static::DATA_SCOPE_UPSELL
                ),
                static::DATA_SCOPE_UPSELL => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_UPSELL),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 35,
                    ],
                ],
            ]
        ];
    }

    public function addFormCrossSells()
    {
        $block = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Catalog\Product\Edit\Tab\CrossSells');
        return [
            'children' => [
                'BPP' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => null,
                                'formElement' => \Magento\Ui\Component\Container::NAME,
                                'componentType' => \Magento\Ui\Component\Container::NAME,
                                'template' => 'ui/form/components/complex',
                                'sortOrder' => 40,
                                'content' => $block->toHtml(),
                            ],
                        ],
                    ],
                ]
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 40,
                    ],
                ],
            ]
        ];
    }



    public function addTreeCategoryCrossSells()
    {
        $block = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Block\Adminhtml\Category\Tree');
        $block->setTreeId('_itoris_3');
        $block->setTreeLinkId('_crosssells');

        $categoryModelFactory = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Model\CategoryFactory');

        $categoryModel = $categoryModelFactory->create();
        $categoryCollection = $categoryModel->getCollection();
        $loadCategory = $categoryCollection->addFieldToFilter( 'product_id', $block->getRequestProductId() )
            ->addFieldToFilter('sells_id', '3')->addFieldToFilter( 'store_id', $block->getStore()->getId() )->getData();

        $productModelFactory = $this->getObjectManager()->create('Itoris\AutoRelatedUpsellsCrossSells\Model\ProductFactory');
        $productModel = $productModelFactory->create();
        $productCollection = $productModel->getCollection();
        $loadProduct = $productCollection->addFieldToFilter( 'product_id', $block->getRequestProductId() )
            ->addFieldToFilter('sells_id', '3')->addFieldToFilter( 'store_id', $block->getStore()->getId() )->getData();

        $attrProductArray = [];
        foreach( $loadProduct as $item){
            $attrProductArray[$item['attr_code']] = $item['value'];
        }

        if( isset($attrProductArray['same_category']) ){
            $block->setSameCategory(0);
        }else{
            $block->setSameCategory(1);
        }

        if($loadCategory){
            $buff = '';
            foreach ($loadCategory as $item){
                if($buff) {
                    $buff .= ','.$item['category_id'];
                }else{
                    $buff = $item['category_id'];
                }
            }
            $block->setOpenCategory( $buff );

        }else{
            $block->setOpenCategory('1');
        }

        return [
            'children' => [
                'BPP' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => null,
                                'formElement' => \Magento\Ui\Component\Container::NAME,
                                'componentType' => \Magento\Ui\Component\Container::NAME,
                                'template' => 'ui/form/components/complex',
                                'sortOrder' => 45,
                                'content' => $block->toHtml(),
                                'additionalClasses' => 'fieldset',
                            ],
                        ],
                    ],
                ]
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 45,
                    ],
                ],
            ]
        ];
    }

    protected function getCrossSellFieldset()
    {
        if(!$this->getDataHelper()->isEnabled()){
            return parent::getCrossSellFieldset();
        }
        $content = __('');

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Cross-Sell Products'),
                    $this->scopePrefix . static::DATA_SCOPE_CROSSSELL
                ),
                'modal' => $this->getGenericModal(
                    __('Add Cross-Sell Products'),
                    $this->scopePrefix . static::DATA_SCOPE_CROSSSELL
                ),
                static::DATA_SCOPE_CROSSSELL => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_CROSSSELL),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => false,
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 50,
                    ],
                ],
            ]
        ];

    }

    /**
     * @return \Itoris\AutoRelatedUpsellsCrossSells\Helper\Data
     */
    public function getDataHelper(){
        return \Magento\Framework\App\ObjectManager::getInstance()->create('Itoris\AutoRelatedUpsellsCrossSells\Helper\Data');
    }

    /**
     * @return \Magento\Framework\ObjectManagerInterface
     */
    protected function getObjectManager(){
        return $this->getDataHelper()->getObjectManager();
    }

    protected function getButtonSet(Phrase $content, Phrase $buttonTitle, $scope)
    {
        if(!$this->getDataHelper()->isEnabled()){
            return parent::getButtonSet($content, $buttonTitle, $scope);
        }
        $modalTarget = $this->scopeName . '.' . static::GROUP_RELATED . '.' . $scope . '.modal';

        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'component' => 'Magento_Ui/js/form/components/button',
                        'actions' => [
                            [
                                'targetName' => $modalTarget,
                                'actionName' => 'toggleModal',
                            ],
                            [
                                'targetName' => $modalTarget . '.' . $scope . '_product_listing',
                                'actionName' => 'render',
                            ]
                        ],
                        'title' => $buttonTitle,
                        'provider' => null,
                    ],
                ],
            ],

        ];
    }

}