<?php
namespace FME\Productattachments\Block\Adminhtml\Renderer;

use \Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\Data\CollectionDataSourceInterface;

class Filetype extends AbstractRenderer implements CollectionDataSourceInterface
{


    /**
     * constructor
     *
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }//end __construct()


    public function render(\Magento\Framework\DataObject $row)
    {
        $downlaod_link = $row->getData('file_icon');
        $number        = $row->getData('file_type');
        $result        = $downlaod_link.' '.'( .'.$number.' )';

        return $result;
    }//end render()
}//end class
