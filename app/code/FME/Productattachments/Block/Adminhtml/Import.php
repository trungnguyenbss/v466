<?php
namespace FME\Productattachments\Block\Adminhtml;

use Magento\Backend\Block\Widget\Tabs;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Json\EncoderInterface;
use Magento\Backend\Model\Auth\Session;
use FME\Productattachments\Helper\Data;

class Import extends Tabs
{

    public $helper;

    public $path = '/productattachments/';


    public function __construct(
        Context $context,
        EncoderInterface $encoderInterface,
        Session $authSession,
        Data $helper
    ) {
        parent::__construct($context, $encoderInterface, $authSession);
        $this->helper = $helper;
        $this->setTemplate('FME_Productattachments::import.phtml');
        $this->setFormAction($this->_urlBuilder->getUrl('*/import/import'));
    }//end __construct()


    protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }//end _beforeToHtml()
}//end class
