<?php
namespace FME\Productattachments\Block\Adminhtml;

class Productcats extends \Magento\Backend\Block\Widget\Grid\Container
{
    public function _construct()
    {
        $this->_controller     = 'adminhtml_productcats';
        $this->_blockGroup     = 'FME_Productattachments';
        $this->_headerText     = __('Product Attachments Category Manager');
        $this->_addButtonLabel = __('Add Category');
        parent::_construct();
    }//end _construct()
}//end class
