/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    map: {
        "*": {
            prettyphoto: 'FME_Productattachments/js/jquery.prettyPhoto',
            pajquery: 'FME_Productattachments/js/jquery-1.6.1.min'
        }
    },
    paths: {
        "prettyphoto": 'js/jquery.prettyPhoto',
        "pajquery": 'js/jquery-1.6.1.min'
    },
    shim: {
        "prettyphoto": {
            deps: ["pajquery"]
        }
    }
};
