<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace FME\Productattachments\Model\Config\Source;

class cmspages extends \FME\Productattachments\Model\Productattachments implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        return $this->getCMSPage();
    }//end toOptionArray()
}//end class
