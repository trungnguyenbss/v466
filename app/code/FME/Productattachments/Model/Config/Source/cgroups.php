<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace FME\Productattachments\Model\Config\Source;

class cgroups extends \FME\Productattachments\Model\Productcats implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        return $this->getcgroups();
    }//end toOptionArray()
}//end class
