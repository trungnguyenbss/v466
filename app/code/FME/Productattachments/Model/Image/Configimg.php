<?php
/*
    ////////////////////////////////////////////////////////////////////////////////
    \\\\\\\\\\\\\\\\\\\\\\\\\  FME Productattachments Module  \\\\\\\\\\\\\\\\\\\\\\\\\
    /////////////////////////////////////////////////////////////////////////////////
    \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ///////                                                                   ///////
    \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
    ///////   that is bundled with this package in the file LICENSE.txt.      ///////
    \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
    ///////          http://opensource.org/licenses/osl-3.0.php               ///////
    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ///////                      * @category   FME                            ///////
    \\\\\\\                      * @package    FME_Productattachments              \\\\\\\
    ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
    \\\\\\\                                                                   \\\\\\\
    /////////////////////////////////////////////////////////////////////////////////
    \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
    /////////////////////////////////////////////////////////////////////////////////
 */
namespace FME\Productattachments\Model\Image;

class Configimg extends \Magento\Framework\ObjectManager\ObjectManager
{
    // ---Functions---
    public function getBaseMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl('').'productattachments/images';
    }//end getBaseMediaUrl()


    public function getBaseMediaPath()
    {
        return 'media/productattachments/images';
    }//end getBaseMediaPath()


    public function getMediaUrl($file)
    {
        $aryfile = explode('/', $file);

        return $this->_storeManager->getStore()->getBaseUrl('').'productattachments/images/'.$file;
    }//end getMediaUrl()


    public function getMediaPath($file)
    {
        $aryfile = explode('/', $file);
        return 'media/productattachments/images/'.$file;
    }//end getMediaPath()


    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\ObjectManager\FactoryInterface $factory,
        \Magento\Framework\ObjectManager\ConfigInterface $config
    ) {
                parent::__construct($factory, $config);
                $this->_objectManager = $objectManager;
    }//end __construct()
}//end class
