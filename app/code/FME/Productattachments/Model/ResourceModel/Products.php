<?php

namespace FME\Productattachments\Model\ResourceModel;

class Products extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    
    protected function _construct()
    {
        $this->_init('productattachments_products', 'product_id');
    }
}
