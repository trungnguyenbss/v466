<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Productattachments\Controller\Adminhtml\Productattachments;

use FME\Productattachments\Controller\Adminhtml\AbstractMassDelete;

/**
 * Class MassDelete
 */
class MassDelete extends AbstractMassDelete
{
    /**
     * Field id
     */
    const ID_FIELD = 'productattachments_id';

    /**
     * ResourceModel collection
     *
     * @var string
     */
    protected $collection = 'FME\Productattachments\Model\ResourceModel\Productattachments\Collection';

    /**
     * Page model
     *
     * @var string
     */
    protected $model = 'FME\Productattachments\Model\Productattachments';

    protected $cat = false;
}//end class
