<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\  FME Productattachments Module  \\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   FME                            ///////
 \\\\\\\                      * @package    FME_Productattachments              \\\\\\\
 ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
namespace FME\Productattachments\Controller\Adminhtml\Productattachments;

class Edit extends \FME\Productattachments\Controller\Adminhtml\Productattachments
{
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $id     = $this->getRequest()->getParam('id');
        $model  = $this->_objectManager->create('FME\Productattachments\Model\Productattachments')->load($id);

        if ($model->getId() || $id == 0) {
            $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            $this->_coreRegistry->register('productattachments_data', $model);


            $resultPage = $this->_initAction();
            $resultPage->addBreadcrumb(
                $id ? __('Edit Attachment') : __('New Attachment'),
                $id ? __('Edit Attachment') : __('New Attachment')
            );
            $resultPage->getConfig()->getTitle()->prepend(__('Productattachments'));
            $resultPage->getConfig()->getTitle()
            ->prepend($model->getProductattachmentsId() ? $model->getTitle() : __('New Attachment'));

            return $resultPage;
        } else {
            $this->messageManager->addError($this->_objectManager->get('FME\Productattachments\Helper\Data')->__('File does not exist'));
            $this->_redirect('*/*/');
        }
    }
}
