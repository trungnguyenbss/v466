<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Productattachments\Controller\Adminhtml\Productcats;

use FME\Productattachments\Controller\Adminhtml\AbstractMassStatus;

/**
 * Class MassDelete
 */
class MassDisable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'category_id';

    /**
     * ResourceModel collection
     *
     * @var string
     */
    protected $collection = 'FME\Productattachments\Model\ResourceModel\Productcats\Collection';

    /**
     * Page model
     *
     * @var string
     */
    protected $model = 'FME\Productattachments\Model\Productcats';

    /**
     * Item status
     *
     * @var boolean
     */
    protected $status = 0;
}//end class
