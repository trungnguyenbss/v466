<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Productattachments\Controller\Adminhtml\Productcats\Image;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Upload
 */
class Uploadcats extends \Magento\Backend\App\Action
{

    /**
     * Image uploader
     *
     * @var \Magento\Catalog\Model\ImageUploader
     */
    protected $baseTmpPath;

    protected $imageUploader;

    protected $logger;
   
    /**
     * Upload constructor.
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Catalog\Model\ImageUploader $imageUploader
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \FME\Productattachments\Model\ImageUploader $imageUploader,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->logger    = $logger;
    }//end __construct()


    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return true;
    }//end _isAllowed()


    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $result = $this->imageUploader->saveFileToTmpDir('category_image');

            $result['cookie'] = [
                                 'name'     => $this->_getSession()->getName(),
                                 'value'    => $this->_getSession()->getSessionId(),
                                 'lifetime' => $this->_getSession()->getCookieLifetime(),
                                 'path'     => $this->_getSession()->getCookiePath(),
                                 'domain'   => $this->_getSession()->getCookieDomain(),
                                ];
        } catch (\Exception $e) {
            $result = [
                       'error'     => $e->getMessage(),
                       'errorcode' => $e->getCode(),
                      ];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }//end execute()
}//end class
