<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Productattachments\Controller\Adminhtml\Extensions;

use FME\Productattachments\Controller\Adminhtml\AbstractMassStatus;

/**
 * Class MassDelete
 */
class MassEnable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'extension_id';

    /**
     * ResourceModel collection
     *
     * @var string
     */
    protected $collection = 'FME\Productattachments\Model\ResourceModel\Extensions\Collection';

    /**
     * Page model
     *
     * @var string
     */
    protected $model = 'FME\Productattachments\Model\Extensions';

    /**
     * Item status
     *
     * @var boolean
     */
    protected $status = 1;
}//end class
