<?php
/*
    ////////////////////////////////////////////////////////////////////////////////
    \\\\\\\\\\\\\\\\\\\\\\\\\  FME Productcats Module  \\\\\\\\\\\\\\\\\\\\\\\\\
    /////////////////////////////////////////////////////////////////////////////////
    \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ///////                                                                   ///////
    \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
    ///////   that is bundled with this package in the file LICENSE.txt.      ///////
    \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
    ///////          http://opensource.org/licenses/osl-3.0.php               ///////
    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ///////                      * @category   FME                            ///////
    \\\\\\\                      * @package    FME_Productcats              \\\\\\\
    ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
    \\\\\\\                                                                   \\\\\\\
    /////////////////////////////////////////////////////////////////////////////////
    \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
    /////////////////////////////////////////////////////////////////////////////////
 */

namespace FME\Productattachments\Controller\Adminhtml\Extensions;

class PostDataProcessor
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter
     */
    protected $dateFilter;

    /**
     * @var \Magento\Framework\View\Model\Layout\Update\ValidatorFactory $validatorFactory
     */
    protected $validatorFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface $messageManager
     */
    protected $messageManager;


    /**
     * @param \Magento\Framework\Stdlib\DateTime\Filter\Date               $dateFilter
     * @param \Magento\Framework\Message\ManagerInterface                  $messageManager
     * @param \Magento\Framework\View\Model\Layout\Update\ValidatorFactory $validatorFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Model\Layout\Update\ValidatorFactory $validatorFactory
    ) {
        $this->dateFilter       = $dateFilter;
        $this->messageManager   = $messageManager;
        $this->validatorFactory = $validatorFactory;
    }//end __construct()


    /**
     * Check if required fields is not empty
     *
     * @param  array $data
     * @return boolean
     */
    public function filter($data)
    {
        $inputFilter = new \Zend_Filter_Input(
            [],
            [],
            $data
        );
        $data        = $inputFilter->getUnescaped();
        return $data;
    }//end filter()


    protected function _isAllowed()
    {
        return true;
    }//end _isAllowed()
}//end class
