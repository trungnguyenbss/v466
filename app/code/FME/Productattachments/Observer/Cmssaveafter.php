<?php

namespace FME\Productattachments\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use FME\Productattachments\Model\Productattachments as ProductattachmentsModel;

class Cmssaveafter implements ObserverInterface
{

    
    protected $request;
    protected $productattachmentsModel;
    protected $_objectManager;

    public function __construct(Context $context, ProductattachmentsModel $productattachmentsModel)
    {
        $this->_objectManager = $context->getObjectManager();
        $this->request = $context->getRequest();
        $this->productattachmentsModel = $productattachmentsModel;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
      
        $postData = $this->request->getPost();
        $page_id = $postData['page_id'];
       // echo "<pre>";
      //  echo $page_id.'ddd';
    //  print_r($postData);exit;
         $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                    ->getDirectoryRead(DirectoryList::MEDIA);
            $baseScmsMediaURL = $mediaDirectory->getAbsolutePath();
        $fileconfig = $this->_objectManager->create('FME\Productattachments\Model\Image\Fileicon');
       
        if (isset($postData['page']['attachments'])) {
            foreach ($postData['page']['attachments']['dynamic_rows'] as $key => $value) {
                if (isset($value['attachment_id'])) {
                    $data['productattachments_id'] = $value['attachment_id'];
                    $model = $this->productattachmentsModel->load($value['attachment_id']);
                } else {
                    $data['productattachments_id'] = null;
                    $model = $this->productattachmentsModel;
                }

                if (isset($value['is_delete'])) {
                    if (isset($value['attachment_id'])) {
                        $model_del = $this->_objectManager->create('FME\Productattachments\Model\Productattachments');

                        $model_del->setId($value['attachment_id'])->delete();
                         unset($data);
                         unset($model_del);

                        continue;
                    } else {
                        $data['productattachments_id'] = null;
                        continue;
                    }
                }
            
         

               

                if (isset($value['filename'])) {
                    if ($value['filename'][0]['status'] == 'old') {
                         $temp='';
                    } else {
                        $filepath = $baseScmsMediaURL.'productattachments/files/'.$value['filename'][0]['name'];

                        $data['filename'] = 'productattachments/files/'.$value['filename'][0]['name'];

                        $fileconfig->Fileicon($filepath);
                        $data['download_link'] = $value['filename'][0]['url'];
                        $data['file_icon'] = $fileconfig->displayIcon();
                        $data['file_type'] = $fileconfig->getType();
                        $data['file_size'] = $fileconfig->getSize();
                    }
                }
            
            



             
                    $data['cmspage_ids'] = (array)$page_id;
                    $data['product_id'] = (array)null;
                    $data['title'] = $value['title'];
                    $data['status'] = $value['status'];
                    $data['downloads'] = 0;
             
                     $data['block_position'] = 'additional,other';
                     $data['limit_downloads'] = 0;
            
                     $data['cat_id'] = 1;
                     $data['store_id'] = (array)0;

                if ($value['type'] == 'url') {
                    $data['link_url'] = $value['link_url'];
                    $data['file_icon'] = '';
                    $data['file_type'] = '';
                    $data['file_size'] = '';
                    $data['filename'] = '';
                }

            
                if (isset($value['customer_group'])) {
                    foreach ($value['customer_group'] as $key => $allGroup) {
                          $groups_array[] = $allGroup;
                    }
            
        

                    $cgroup = implode(',', $groups_array);
                    $data['customer_group_id'] = $cgroup;
                    unset($groups_array);
                } else {
                    $data['customer_group_id'] = null;
                }
        
                     $model->setData($data);

             
                if ($model->getCreatedTime() == null || $model->getUpdateTime() == null) {
                      $model->setCreatedTime(date('y-m-d h:i:s'))
                      ->setUpdateTime(date('y-m-d h:i:s'));
                }

                     $model->save();
            }
        }
    }
}
